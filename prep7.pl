#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

# preprocess day7 input for COBOL

sub main {

    my $max_adj = 0;
    my $max_vari = 0;

	my $i;
	while($i = <>) {
		# shiny teal bags contain 3 muted maroon bags, 1 bright salmon bag, 2 dark chartreuse bags.
        my @a = split(/bags contain/, $i); # @a[0] limits+char, @a[1] passwd
        my @b = split(/,/, $a[1]); # @b[0] limits, @b[1] char

        my $count = scalar @b;
        chomp $a[0];
        my ($adj, $vari) = split(/ /, $a[0]);
        #my $string; = $count . " " . $adj . ' ' x (8 - length $adj) . " " . $vari . ' ' x (10 - length $vari) . "  \n";
        
        #print $string;
        for my $m (@b) {
            if($m =~ /(\d{1}) (\w*) (\w*) [bag.|bags.]/) {
                print $adj . ' ' x (8 - length $adj) . " " . $vari . ' ' x (10 - length $vari) . " " . "$1 $2 " . ' ' x (8 - length $2) . "$3" . ' ' x (10 - length $3) . "\n";
            }
            else { print $adj . ' ' x (8 - length $adj) . " " . $vari . ' ' x (10 - length $vari) . " " . "0 \n"; }
        }
        #$string = '';


	}		

}


main()


