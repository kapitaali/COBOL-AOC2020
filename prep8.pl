#!/usr/bin/perl

#use strict;
use warnings;

my $t;

while(<>) {
	my @a = split(/\s/, $_);
    if($a[1] =~ m/(\-|\+)(\d{1,3})/) {
        my ($b, $c) = ($1, $2);
        $t = $a[0] . " " . $b . ' ' x (4 - length $c) . $c; 
        print "$t \n";
    }
}

