       IDENTIFICATION DIVISION.
       PROGRAM-ID. AOCday8.
       ENVIRONMENT DIVISION.
      *
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INPUT-FILE  
              ASSIGN TO 'day_8.txt'
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD  INPUT-FILE RECORDING MODE F.
       01  FILEINPUT-I.
           05 COMMAND-I      PIC X(3).
           05 FILLER         PIC X VALUE SPACE.
           05 ETUMERKKI-I    PIC X.
           05 FILLER         PIC X VALUE SPACE.
           05 ARG-I          PIC 9(3).

       WORKING-STORAGE SECTION.
       01 Teibel. 
         02 Taulukko         OCCURS 1 TO 999 
            DEPENDING ON Indeksi.        
           05 COMMAND        PIC X(3).
           05 FILLER         PIC X VALUE SPACE.
           05 ETUMERKKI      PIC X.
           05 FILLER         PIC X VALUE SPACE.
           05 ARGU           PIC 9(3).

       01 Teibel2. 
         02 Taulukko2        OCCURS 1 TO 999 
            DEPENDING ON Indeksi2.
           05 EXE            PIC 9 VALUE ZEROS.

       01  TEMP-CALC-VALUE.
           05  AKKU-TOTAL    PIC S9(8) VALUE 0.
           05  AA            PIC 9(3).
           05  MAX-ITER      PIC 9 VALUE 0.
           05  ITER          PIC 9(3) VALUE 1.
           
       77  TEMP-CTR          PIC 9(3).
       77  PROJECT-INDEX     PIC 9(3).
       77  Indeksi           PIC 9(3) VALUE 1.
       77  Indeksi2          PIC 9(3) VALUE 1.
       77  TABLE-MAX         PIC 9(3) VALUE 999.
       77  SW-END-OF-FILE    PIC X(01) VALUE SPACES.
                88 END-OF-FILE   VALUE 'Y'.

       PROCEDURE DIVISION.
           PERFORM 000-HOUSEKEEPING.
           PERFORM 100-PROCESS-COMMANDS.
           PERFORM 900-WRAP-UP
           GOBACK.
       000-HOUSEKEEPING.
           INITIALIZE Teibel.
           INITIALIZE Teibel2.
           OPEN INPUT INPUT-FILE.
           READ INPUT-FILE
              AT END MOVE 'Y' TO SW-END-OF-FILE.
           PERFORM VARYING PROJECT-INDEX FROM 1 BY 1
              UNTIL END-OF-FILE
              MOVE FILEINPUT-I TO
                        Taulukko (PROJECT-INDEX)
              ADD 1 TO Indeksi
              READ INPUT-FILE
                  AT END MOVE 'Y' TO  SW-END-OF-FILE
              END-READ
           END-PERFORM.
           DISPLAY " ".

       100-PROCESS-COMMANDS.
           MOVE 1 TO AA.
           PERFORM UNTIL MAX-ITER IS GREATER THAN OR EQUAL TO 2
             EVALUATE COMMAND (AA)
                WHEN "acc"
                   DISPLAY AA " - acc: " AKKU-TOTAL " " COMMAND (AA) 
                    " " ETUMERKKI(AA) ARGU (AA) 
                   IF ETUMERKKI (AA) = '+'
                    COMPUTE AKKU-TOTAL = AKKU-TOTAL + ARGU (AA)
                   ELSE
                    COMPUTE AKKU-TOTAL = AKKU-TOTAL - ARGU (AA)
                   END-IF
                   COMPUTE MAX-ITER = EXE (AA) + 1
                   ADD 1 TO EXE (AA)
                   ADD 1 TO AA
                WHEN "jmp"
                   DISPLAY AA " - acc: " AKKU-TOTAL " " COMMAND (AA) 
                    " " ETUMERKKI(AA) ARGU (AA)                
                   COMPUTE MAX-ITER = EXE (AA) + 1
                   ADD 1 TO EXE (AA)    
                   IF ETUMERKKI (AA) = '+'
                    COMPUTE AA = AA + ARGU (AA)    
                   ELSE
                    COMPUTE AA = AA - ARGU (AA)    
                   END-IF
                WHEN "nop"
                   DISPLAY AA " - acc: " AKKU-TOTAL " " COMMAND (AA) 
                    " "ETUMERKKI(AA) ARGU (AA)                
                   COMPUTE MAX-ITER = EXE (AA) + 1
                   ADD 1 TO EXE (AA)
                   ADD 1 TO AA
                   EXIT
             END-EVALUATE
           END-PERFORM.

       900-WRAP-UP.
           CLOSE INPUT-FILE.
           DISPLAY "akkumulaattori: " AKKU-TOTAL 
           DISPLAY "------------------------------------------"
           DISPLAY " ".
