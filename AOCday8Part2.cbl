       IDENTIFICATION DIVISION.
       PROGRAM-ID. AOCday8.
       ENVIRONMENT DIVISION.
      *
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INPUT-FILE  
              ASSIGN TO 'day_8.txt'
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD  INPUT-FILE RECORDING MODE F.
       01  FILEINPUT-I.
           05 COMMAND-I      PIC X(3).
           05 FILLER         PIC X VALUE SPACE.
           05 ETUMERKKI-I    PIC X.
           05 FILLER         PIC X VALUE SPACE.
           05 ARG-I          PIC 9(3).

       WORKING-STORAGE SECTION.
       01 Teibel. 
         02 Taulukko         OCCURS 1 TO 2000 
            DEPENDING ON Indeksi.        
           05 COMMAND        PIC X(3).
           05 FILLER         PIC X VALUE SPACE.
           05 ETUMERKKI      PIC X.
           05 FILLER         PIC X VALUE SPACE.
           05 ARGU           PIC 9(3).

       01 Original. 
         02 O-taulukko         OCCURS 1 TO 2000 
            DEPENDING ON Indeksi3.        
           05 O-COMMAND      PIC X(3).
           05 FILLER         PIC X VALUE SPACE.
           05 O-ETUMERKKI    PIC X.
           05 FILLER         PIC X VALUE SPACE.
           05 O-ARGU         PIC 9(3).

       01 Teibel2. 
         02 Taulukko2        OCCURS 1 TO 2000 
            DEPENDING ON Indeksi2.
           05 EXE            PIC 9 VALUE ZEROS.

       01 Jmptable. 
         02 Jmptaulukko      OCCURS 1 TO 2000 
            DEPENDING ON Indeksi-jmp.
           05 JMPIT          PIC 9(4) VALUE ZEROS.

       01 Noptable. 
         02 Noptaulukko      OCCURS 1 TO 2000 
            DEPENDING ON Indeksi-nop.
           05 NOPIT          PIC 9(4) VALUE ZEROS.

       01  TEMP-CALC-VALUE.
           05  AKKU-TOTAL    PIC S9(4) VALUE 0.
           05  AA            PIC 9(4).
           05  MAX-ITER      PIC 9 VALUE 0.
           05  ITER          PIC 9(4) VALUE 1.
           05  ITER3         PIC 9(3) VALUE 1.
           
       77  TEMP-CTR          PIC 9(4).
       77  PROJECT-INDEX     PIC 9(4).
       77  Indeksi           PIC 9(4) VALUE 1.
       77  Indeksi2          PIC 9(4) VALUE 1.
       77  Indeksi3          PIC 9(4) VALUE 1.       
       77  Indeksi-jmp       PIC 9(4) VALUE 1.
       77  Indeksi-nop       PIC 9(4) VALUE 1.       
       77  TABLE-MAX         PIC S9(4) VALUE 2000.
       77  SW-END-OF-FILE    PIC X(01) VALUE SPACES.
                88 END-OF-FILE   VALUE 'Y'.

       PROCEDURE DIVISION.
           PERFORM 000-HOUSEKEEPING.
           PERFORM 200-COPY-ORIGINAL.           
           PERFORM 300-FIND-JMP-NOP.
      *     PERFORM 800-PRINT-NOPS.
      *     PERFORM 801-PRINT-JMPS.
           PERFORM VARYING PROJECT-INDEX FROM 1 BY 1 
            UNTIL PROJECT-INDEX = Indeksi-nop  
              DISPLAY "vaihdetaan nop -> jmp kohdassa " TEMP-CTR
              MOVE Noptaulukko (PROJECT-INDEX) TO TEMP-CTR
              MOVE 'jmp' TO COMMAND (TEMP-CTR)
              PERFORM 100-PROCESS-COMMANDS
      *        DISPLAY "akkumulaattori: " AKKU-TOTAL              
      *        DISPLAY "----------------"
              PERFORM 200-COPY-ORIGINAL
           END-PERFORM.
           PERFORM VARYING PROJECT-INDEX FROM 1 BY 1 
            UNTIL PROJECT-INDEX = Indeksi-jmp  
              DISPLAY "vaihdetaan jmp -> nop kohdassa " TEMP-CTR            
              MOVE Jmptaulukko (PROJECT-INDEX) TO TEMP-CTR
              MOVE 'nop' TO COMMAND (TEMP-CTR)
              PERFORM 100-PROCESS-COMMANDS
      *        DISPLAY "akkumulaattori: " AKKU-TOTAL
      *        DISPLAY "----------------"
              PERFORM 200-COPY-ORIGINAL
           END-PERFORM.           
           PERFORM 900-WRAP-UP.
           GOBACK.

       000-HOUSEKEEPING.
           INITIALIZE Teibel.
           INITIALIZE Teibel2.
           INITIALIZE Original.
           INITIALIZE Jmptable.
           INITIALIZE Noptable.
           OPEN INPUT INPUT-FILE.
           READ INPUT-FILE
              AT END MOVE 'Y' TO SW-END-OF-FILE.
           PERFORM VARYING PROJECT-INDEX FROM 1 BY 1
              UNTIL END-OF-FILE
              MOVE FILEINPUT-I TO
                    Taulukko (PROJECT-INDEX)
              ADD 1 TO Indeksi
              MOVE FILEINPUT-I TO 
                    O-taulukko (PROJECT-INDEX)
              ADD 1 TO Indeksi3
              READ INPUT-FILE
                  AT END MOVE 'Y' TO  SW-END-OF-FILE
              END-READ
           END-PERFORM.
           DISPLAY Indeksi " records read".
          
       100-PROCESS-COMMANDS.
           MOVE 0 TO AKKU-TOTAL.
           MOVE ZEROES TO Teibel2.
           PERFORM VARYING ITER FROM 1 BY 1 UNTIL ITER = Indeksi 
              MOVE 0 TO EXE (ITER)
           END-PERFORM.
           MOVE 1 TO AA.
           MOVE 0 TO MAX-ITER.
           PERFORM UNTIL MAX-ITER IS GREATER THAN OR EQUAL TO 2
             IF AA = Indeksi - 1
              DISPLAY "Found it! This is the iteration to look for!"
              DISPLAY "accumulator value: " AKKU-TOTAL 
              CLOSE INPUT-FILE
              GOBACK
             END-IF
             EVALUATE COMMAND (AA)
                WHEN "acc"
      *             DISPLAY AA " - acc: " AKKU-TOTAL " " COMMAND (AA) 
      *              " " ETUMERKKI(AA) ARGU (AA) " " EXE (AA)
                   IF ETUMERKKI (AA) = '+'
                    COMPUTE AKKU-TOTAL = AKKU-TOTAL + ARGU (AA)
                   ELSE
                    COMPUTE AKKU-TOTAL = AKKU-TOTAL - ARGU (AA)
                   END-IF
                   COMPUTE MAX-ITER = EXE (AA) + 1
                   ADD 1 TO EXE (AA)
                   ADD 1 TO AA
                WHEN "jmp"
      *             DISPLAY AA " - acc: " AKKU-TOTAL " " COMMAND (AA) 
      *              " " ETUMERKKI(AA) ARGU (AA) " " EXE (AA)               
                   COMPUTE MAX-ITER = EXE (AA) + 1
                   ADD 1 TO EXE (AA)    
                   IF ETUMERKKI (AA) = '+'
                    COMPUTE AA = AA + ARGU (AA)    
                   ELSE
                    COMPUTE AA = AA - ARGU (AA)    
                   END-IF
                WHEN "nop"
      *             DISPLAY AA " - acc: " AKKU-TOTAL " " COMMAND (AA) 
      *              " " ETUMERKKI(AA) ARGU (AA) " " EXE (AA)                
                   COMPUTE MAX-ITER = EXE (AA) + 1
                   ADD 1 TO EXE (AA)
                   ADD 1 TO AA
                   EXIT
             END-EVALUATE
           END-PERFORM.

       200-COPY-ORIGINAL.
           PERFORM VARYING ITER FROM 1 BY 1 UNTIL ITER = Indeksi 
              MOVE O-COMMAND (ITER) TO COMMAND (ITER)
              MOVE O-ETUMERKKI (ITER) TO ETUMERKKI (ITER)
              MOVE O-ARGU (ITER) TO ARGU (ITER)
           END-PERFORM.

       300-FIND-JMP-NOP.
           PERFORM VARYING ITER FROM 1 BY 1 UNTIL ITER = Indeksi 
              EVALUATE COMMAND (ITER)
                 WHEN "nop"
                    MOVE ITER TO Noptaulukko (Indeksi-nop)
                    ADD 1 TO Indeksi-nop
      *              DISPLAY ITER ": " COMMAND (ITER) " " ARGU (ITER)
                 WHEN "jmp"
                    MOVE ITER TO Jmptaulukko (Indeksi-jmp)
                    ADD 1 TO Indeksi-jmp
      *              DISPLAY ITER ": " COMMAND (ITER) " " ARGU (ITER)
              END-EVALUATE
           END-PERFORM.       

       800-PRINT-NOPS.
           PERFORM VARYING ITER FROM 1 BY 1 UNTIL ITER = Indeksi-nop 
              MOVE Noptaulukko (ITER) TO TEMP-CTR
              DISPLAY Noptaulukko (ITER) " " COMMAND (TEMP-CTR)
           END-PERFORM.

       801-PRINT-JMPS.
           PERFORM VARYING ITER FROM 1 BY 1 UNTIL ITER = Indeksi-jmp 
              MOVE Jmptaulukko (ITER) TO TEMP-CTR
              DISPLAY Jmptaulukko (ITER) " " COMMAND (TEMP-CTR)
           END-PERFORM.      

       900-WRAP-UP.
           CLOSE INPUT-FILE.
           DISPLAY "akkumulaattori: " AKKU-TOTAL 
           DISPLAY "------------------------------------------"
           DISPLAY " ".
