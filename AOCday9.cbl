       IDENTIFICATION DIVISION.
       PROGRAM-ID. AOCday9.
       ENVIRONMENT DIVISION.
      *
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INPUT-FILE  
              ASSIGN TO 'day_9.txt'
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD  INPUT-FILE RECORDING MODE F.
       01  FILEINPUT-I.
           05 LUKU-I      PIC 9(20).

       WORKING-STORAGE SECTION.
       01 Teibel. 
         02 Taulukko         OCCURS 1 TO 2000 
            DEPENDING ON Indeksi.        
           05 LUKU           PIC 9(20).

       01 Teibel2. 
         02 Summattavat      OCCURS 25 TIMES.        
           05 LUKU2          PIC 9(20).

       01  TEMP-CALC-VALUE.
           05  SUMMA         PIC 9(20) VALUE 0.
           05  VALISUMMA     PIC 9(20) VALUE 0.
           05  APUSUMMA      PIC 9(20) VALUE 0.
           05  MONTAKO       PIC 9(4)  VALUE 0. 
           05  AA            PIC 9(4).
           05  BB            PIC 9(4).
           05  IS-FOUND      PIC 9 VALUE 1.
           05  ITER          PIC 9(4) VALUE 1.
           05  KORVATTAVA    PIC 9(2) VALUE 1.
           05  TSEK          PIC 9(20).
           
       77  TEMP-CTR          PIC 9(4).
       77  PROJECT-INDEX     PIC 9(4).
       77  Indeksi           PIC 9(4) VALUE 1.
       77  Indeksi2          PIC 9(4) VALUE 1.
       77  TABLE-MAX         PIC 9(4) VALUE 999.
       77  SW-END-OF-FILE    PIC X(01) VALUE SPACES.
                88 END-OF-FILE   VALUE 'Y'.

       PROCEDURE DIVISION.
           PERFORM 000-HOUSEKEEPING.
      *     PERFORM 100-PROCESS-TABLE.
           PERFORM 300-CALCULATE-SUMS.
           PERFORM 900-WRAP-UP.
           GOBACK.
       000-HOUSEKEEPING.
           INITIALIZE Teibel.
           INITIALIZE Teibel2.
           OPEN INPUT INPUT-FILE.
           READ INPUT-FILE
              AT END MOVE 'Y' TO SW-END-OF-FILE.
           PERFORM VARYING PROJECT-INDEX FROM 1 BY 1
              UNTIL END-OF-FILE
              MOVE LUKU-I TO
                        LUKU (PROJECT-INDEX)
              ADD 1 TO Indeksi
              READ INPUT-FILE
                  AT END MOVE 'Y' TO  SW-END-OF-FILE
              END-READ
           END-PERFORM.
           DISPLAY " ".

       100-PROCESS-TABLE.
           PERFORM VARYING ITER FROM 1 BY 1 UNTIL ITER > 25
              MOVE LUKU (ITER) TO LUKU2 (ITER)
           END-PERFORM.         
           PERFORM VARYING ITER FROM 26 BY 1 UNTIL IS-FOUND = 0 
              MOVE LUKU (ITER) TO TSEK
              PERFORM 200-FIND-SUM
           END-PERFORM.

       200-FIND-SUM.
           PERFORM VARYING AA FROM 1 BY 1 UNTIL AA > 25
              PERFORM VARYING BB FROM 1 BY 1 UNTIL BB > 25
                 COMPUTE SUMMA = LUKU2 (AA) + LUKU2 (BB)
      *           DISPLAY LUKU2(AA) "+" LUKU2(BB) "=" SUMMA
                 IF SUMMA = TSEK
                    MOVE TSEK TO LUKU2 (KORVATTAVA)
                    IF KORVATTAVA = 25
                       MOVE 1 TO KORVATTAVA
                    ELSE
                       ADD 1 TO KORVATTAVA
                    END-IF
                    EXIT PARAGRAPH
                 END-IF
              END-PERFORM
           END-PERFORM.
           DISPLAY "Found! The number is " TSEK.
           MOVE 0 TO IS-FOUND.

       300-CALCULATE-SUMS.
           DISPLAY "ASDF".
           MOVE 104054607 TO TSEK.
           PERFORM VARYING MONTAKO FROM 1 BY 1 UNTIL VALISUMMA = TSEK
              PERFORM VARYING AA FROM 1 BY 1 UNTIL AA = 558
                 MOVE 0 TO APUSUMMA
                 COMPUTE TEMP-CTR = AA + MONTAKO
                 PERFORM VARYING BB FROM AA BY 1 UNTIL BB = TEMP-CTR 
                    ADD Taulukko (BB) TO APUSUMMA
                    DISPLAY "jeeee " APUSUMMA 
                 END-PERFORM
                 MOVE APUSUMMA TO VALISUMMA
                 IF VALISUMMA = TSEK
                    DISPLAY "Found! It's the numbers from " LUKU(AA)
                    DISPLAY "and up till " LUKU(TEMP-CTR) 
                 END-IF
              END-PERFORM
           END-PERFORM.     

       900-WRAP-UP.
           CLOSE INPUT-FILE.
           DISPLAY " ".
