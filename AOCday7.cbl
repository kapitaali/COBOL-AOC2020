       IDENTIFICATION DIVISION.
       PROGRAM-ID. AOCday7.
       ENVIRONMENT DIVISION.
      *
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INPUT-FILE  
              ASSIGN TO 'day7.txt'
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD  INPUT-FILE RECORDING MODE F.
       01  FILEINPUT-I.
           05 ADJ-I          PIC X(8).
           05 FILLER         PIC X VALUE SPACE.
           05 COLOR-I        PIC X(10).
           05 FILLER         PIC X VALUE SPACE.
           05 MONTAKO-I      PIC 9.
           05 FILLER         PIC X VALUE SPACE.
           05 CONT-ADJ-I     PIC X(8).
           05 FILLER         PIC X VALUE SPACE.
           05 CONT-COLOR-I   PIC X(10).

       WORKING-STORAGE SECTION.
       01 Teibel. 
         02 Taulukko         OCCURS 1 TO 2000 
            DEPENDING ON Indeksi.        
           05 ADJ-T          PIC X(8).
           05 FILLER         PIC X VALUE SPACE.
           05 COLOR-T        PIC X(10).
           05 FILLER         PIC X VALUE SPACE.
           05 MONTAKO-T      PIC 9.
           05 FILLER         PIC X VALUE SPACE.
           05 CONT-ADJ-T     PIC X(8).
           05 FILLER         PIC X VALUE SPACE.
           05 CONT-COLOR-T   PIC X(10).

       01 Teibel2. 
         02 Taulukko2        OCCURS 1 TO 2000 
            DEPENDING ON Indeksi2.
           05 ADJ2           PIC X(8).
           05 FILLER         PIC X VALUE SPACE.
           05 COLOR2         PIC X(10).

       01  TEMP-CALC-VALUE.
           05  SUMMA         PIC 9(4) VALUE 0.
           05  CHECK-ADJ     PIC X(8).
           05  CHECK-COLOR   PIC X(10).
           05  IS-FOUND      PIC 9    VALUE 0.
           05  ITER          PIC 9(4) VALUE 1.
           
       77  TEMP-CTR          PIC 9(4).
       77  PROJECT-INDEX     PIC 9(4).
       77  Indeksi           PIC 9(4) VALUE 1.
       77  Indeksi2          PIC 9(4) VALUE 1.
       77  TABLE-MAX         PIC S9(4) VALUE 2000.
       77  SW-END-OF-FILE    PIC X(01) VALUE SPACES.
                88 END-OF-FILE   VALUE 'Y'.

       PROCEDURE DIVISION.
           PERFORM 000-HOUSEKEEPING.
           PERFORM 100-FIND-SHINY-GOLD.
           PERFORM 200-PROCESS-TABLE.
           PERFORM 900-WRAP-UP
           GOBACK.
       000-HOUSEKEEPING.
           INITIALIZE Teibel.
           INITIALIZE Teibel2.
           OPEN INPUT INPUT-FILE.
           READ INPUT-FILE
              AT END MOVE 'Y' TO SW-END-OF-FILE.
           PERFORM VARYING PROJECT-INDEX FROM 1 BY 1
              UNTIL END-OF-FILE
              MOVE FILEINPUT-I TO
                        Taulukko (PROJECT-INDEX)
              ADD 1 TO Indeksi
              READ INPUT-FILE
                  AT END MOVE 'Y' TO  SW-END-OF-FILE
              END-READ
      *          DISPLAY EMP-PROJECT-ITEM(PROJECT-INDEX)
           END-PERFORM.
           DISPLAY " ".

       100-FIND-SHINY-GOLD.
           COMPUTE TEMP-CTR = Indeksi - 1.
           DISPLAY "Records read: " TEMP-CTR.
           PERFORM VARYING PROJECT-INDEX FROM 1 BY 1 
              UNTIL PROJECT-INDEX = Indeksi 
              IF CONT-ADJ-T (PROJECT-INDEX) = 'shiny' AND
                CONT-COLOR-T (PROJECT-INDEX) = 'gold'
                 MOVE ADJ-T (PROJECT-INDEX) TO ADJ2 (Indeksi2)
                 MOVE COLOR-T (PROJECT-INDEX) TO COLOR2 (Indeksi2)
                 ADD 1 TO Indeksi2
              END-IF
           END-PERFORM.

       200-PROCESS-TABLE.
        PERFORM VARYING PROJECT-INDEX FROM 1 BY 1 
        UNTIL PROJECT-INDEX = Indeksi2 
         PERFORM VARYING TEMP-CTR FROM 1 BY 1 
          UNTIL TEMP-CTR = Indeksi
            IF ADJ2 (PROJECT-INDEX) = CONT-ADJ-T (TEMP-CTR) AND 
                 COLOR2 (PROJECT-INDEX) = CONT-COLOR-T (TEMP-CTR)
              MOVE 0 TO IS-FOUND
              MOVE ADJ-T (TEMP-CTR) TO CHECK-ADJ
              MOVE COLOR-T (TEMP-CTR) TO CHECK-COLOR
              PERFORM 300-ADD-IF-NOT-FOUND
            END-IF
         END-PERFORM
        END-PERFORM.

       300-ADD-IF-NOT-FOUND.
           PERFORM VARYING ITER FROM 1 BY 1 UNTIL ITER = Indeksi2 
              IF ADJ2 (ITER) = CHECK-ADJ 
               AND COLOR2 (ITER) = CHECK-COLOR 
                 MOVE 1 TO IS-FOUND 
                 EXIT PERFORM 
              END-IF
           END-PERFORM.
           IF IS-FOUND = 0
              MOVE ADJ-T (TEMP-CTR) TO ADJ2 (Indeksi2)
              MOVE COLOR-T (TEMP-CTR) TO COLOR2 (Indeksi2)
              ADD 1 TO Indeksi2
           END-IF.

       900-WRAP-UP.
           CLOSE INPUT-FILE.
           PERFORM VARYING PROJECT-INDEX FROM 1 BY 1 
              UNTIL PROJECT-INDEX = Indeksi2 
              DISPLAY ADJ2(PROJECT-INDEX) COLOR2(PROJECT-INDEX)
           END-PERFORM.
           COMPUTE SUMMA = Indeksi2 - 1
           DISPLAY "------------------------------------------"
           DISPLAY "total: " SUMMA 
           DISPLAY " ".
